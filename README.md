# Тестовые задачи по SQL

## Задача:
У вас SQL база с таблицами:
1) Users(userId, age)
2) Purchases (purchaseId, userId, itemId, date)
3) Items (itemId, price).

Требуется написать SQL запросы для расчета следующих метрик:

### А) какую сумму в среднем в месяц тратят:
- пользователи в возрастном диапазоне от 18 до 25 лет включительно
- пользователи в возрастном диапазоне от 26 до 35 лет включительно

### Вопросы для уточнения:

Условие задачи неоднозначно. Вопросы для уточнения:

- речь идет про средний показатель по всем пользователям 18-25 лет или необходимо рассчитать и вывести средние для всех пользователей по отдельности?
- необходимо ли выполнить обе задачи одним запросом или это могут быть два разных запроса?

### Решение:

Для самого простого варианта (среднее рассчитывается по всем пользователям данной возрастной группы в сумме, задачу можно выполнять в два запроса по отдельности:)

``` PostgreSQL
SELECT  EXTRACT(MONTH FROM purchases.date) AS "месяц", ROUND(AVG(items.price),2) AS "средняя сумма для 18-25 лет"
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
LEFT JOIN users ON purchases.userId = users.userId 
WHERE users.age BETWEEN 18 AND 25 
GROUP BY EXTRACT(MONTH FROM purchases.date)
ORDER BY "месяц"
```

(сумма округляется до 2 знака после запятой)

Для пользователей 26-35 лет достаточно заменить условие WHERE и название колонки:

``` PostgreSQL
SELECT  EXTRACT(MONTH FROM purchases.date) AS "месяц", ROUND(AVG(items.price),2) AS "средняя сумма для 26-35 лет"
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
LEFT JOIN users ON purchases.userId = users.userId 
WHERE users.age BETWEEN 26 AND 35 
GROUP BY EXTRACT(MONTH FROM purchases.date)
ORDER BY "месяц"
```

Если необходимо рассчитывать среднее для каждого пользователя по отдельности, необходимо будет добавить поле purchases.userId в GROUP BY и добавить его в SELECT  

Если же требуется вывести обе метрики одним запросом в одну таблицу, то необходимо "обернуть" оба этих запроса в подзапросы или временные таблицы и объединить их через FULL OUTER JOIN

``` PostgreSQL
SELECT t1825.mon AS "месяц", 
ROUND(mid1825,2) AS "средняя сумма для 18-25 лет", 
ROUND(mid2635,2) AS "средняя сумма для 26-35 лет" FROM
(
SELECT EXTRACT(MONTH FROM purchases.date) AS mon , AVG(items.price) AS mid1825
FROM purchases
LEFT JOIN items ON purchases.itemId = items.itemId 
LEFT JOIN users ON purchases.userId = users.userId 
WHERE users.age BETWEEN 18 AND 25 
GROUP BY EXTRACT(MONTH FROM purchases.date) ) AS t1825 
FULL OUTER JOIN
(
SELECT EXTRACT(MONTH FROM purchases.date) AS mon, AVG(items.price) AS mid2635
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
LEFT JOIN users ON purchases.userId = users.userId 
WHERE users.age BETWEEN 26 AND 35 
GROUP BY EXTRACT(MONTH FROM purchases.date) ) AS t2635 
ON t1825.mon = t2635.mon
```

MySQL не поддерживает FULL JOIN, а версии раньше 8 не поддерживают и WITH, поэтому такую операцию в MySQL ранних версий можно реализовать, выполнив отдельно LEFT JOIN и RIGHT JOIN и объединив их через UNION:

``` MySQL 5.7
SELECT t1825.mon AS 'месяц', ROUND(mid1825,2) AS 'средняя сумма покупок пользователей 18-25 лет', ROUND(mid2635,2) AS 'средняя сумма покупок пользователей 26-35 лет' FROM
(
SELECT  MONTH(purchases.date) AS mon , AVG(`items`.`price`) AS mid1825
FROM `purchases` 
LEFT JOIN `items` ON `purchases`.itemId = `items`.itemId 
LEFT JOIN `users` ON `purchases`.userId = `users`.userId 
WHERE `users`.`age` BETWEEN 18 AND 25 
GROUP BY MONTH(purchases.date) ) AS t1825 
LEFT JOIN
(
SELECT  MONTH(purchases.date) AS mon , AVG(items.price) AS mid2635
FROM `purchases` 
LEFT JOIN `items` ON `purchases`.itemId = `items`.itemId 
LEFT JOIN `users` ON `purchases`.userId = `users`.userId 
WHERE `users`.`age` BETWEEN 26 AND 35 
GROUP BY MONTH(purchases.date) ) AS t2635 
ON t1825.mon = t2635.mon

UNION

SELECT t2635.mon AS mon, ROUND(mid1825,2), ROUND(mid2635,2) FROM
(
SELECT  MONTH(purchases.date) AS mon , AVG(`items`.`price`) AS mid1825
FROM `purchases` 
LEFT JOIN `items` ON `purchases`.itemId = `items`.itemId 
LEFT JOIN `users` ON `purchases`.userId = `users`.userId 
WHERE `users`.`age` BETWEEN 18 AND 25 
GROUP BY MONTH(purchases.date) ) AS t1825 
RIGHT JOIN
(
SELECT  MONTH(purchases.date) AS mon , AVG(items.price) AS mid2635
FROM `purchases` 
LEFT JOIN `items` ON `purchases`.itemId = `items`.itemId 
LEFT JOIN `users` ON `purchases`.userId = `users`.userId 
WHERE `users`.`age` BETWEEN 26 AND 35 
GROUP BY MONTH(purchases.date) ) AS t2635 
ON t1825.mon = t2635.mon

ORDER BY 'месяц'
```

### Тесты:

Для тестирования желательно, чтобы в тестовой базе были отражены ситуации:
- несколько покупок у одного пользователя за один месяц
- несколько покупок у разных пользователей за один месяц
- покупки у одного пользователя в разных месяцах
- покупки (одного или нескольких пользователей) в одном месяце, но в разные годы
- покупки у пользователей в разных возрастных диапазонах - 18-25 лет, 26-35 лет, и не попадающих в эти интрвалы
- существуют пользователи "граничных" возрастов - 18, 25, 26, 35 лет

#### Код для создания структуры таблиц и заполнения простой тестовой базы

``` PostgreSQL
--
-- Структура таблицы `items`
--

CREATE TABLE items (
  itemId serial NOT NULL primary key,
  price integer NOT NULL CHECK (price > 0)
);

--
-- Дамп данных таблицы `items`
--

INSERT INTO items (itemId, price) VALUES
(1, 100),
(2, 200),
(3, 400),
(4, 1500),
(5, 8099),
(6, 75),
(7, 9000),
(8, 239);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE users (
  userId serial NOT NULL primary key,
  age integer NOT NULL CHECK (age > 0)
)

--
-- Дамп данных таблицы `users`
--

INSERT INTO users (userId, age) VALUES
(1, 22),
(2, 23),
(3, 19),
(4, 35),
(5, 75);

-- --------------------------------------------------------

--
-- Структура таблицы `purchases`
--

CREATE TABLE purchases (
  purchaseId serial NOT NULL primary key,
  userId integer NOT NULL CHECK (userId > 0) REFERENCES users,
  itemId integer NOT NULL CHECK (itemId > 0) REFERENCES items,
  date date NOT NULL
)

--
-- Дамп данных таблицы `purchases`
--

INSERT INTO purchases (purchaseId, userId, itemId, date) VALUES
(1, 1, 2, '2022-01-20'),
(2, 1, 1, '2021-05-30'),
(3, 2, 2, '2021-06-18'),
(4, 3, 2, '2021-07-15'),
(5, 1, 3, '2021-05-20'),
(6, 1, 2, '2021-05-11'),
(7, 1, 3, '2022-01-12'),
(8, 2, 7, '2022-01-22'),
(9, 3, 6, '2022-05-23'),
(10, 4, 7, '2022-05-17'),
(11, 4, 4, '2022-04-27');

```

### Б) в каком месяце года выручка от пользователей в возрастном диапазоне 35+ самая большая

``` PostgreSQL
SELECT EXTRACT(MONTH FROM purchases.date) AS "месяц" , SUM(items.price) AS "сумма" 
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
LEFT JOIN users ON purchases.userId = users.userId 
WHERE users.age >= 35
GROUP BY EXTRACT(MONTH FROM purchases.date)
ORDER BY SUM(items.price) DESC
LIMIT 1

```
### Тесты:

На приведенном выше тестовом наборе данных результат будет тривиальным. Необходимо создать аналогичный набор с пользователями старше 35 лет, либо можно тестировать для возрастной группы 18+ (например), но не забыть потом изменить это число в запросе

### В) какой товар обеспечивает дает наибольший вклад в выручку за последний год

``` PostgreSQL
SELECT purchases.itemId AS "id товара" , SUM(items.price) AS "сумма" 
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
WHERE EXTRACT(YEAR FROM purchases.date) = EXTRACT(YEAR FROM CURRENT_DATE)
GROUP BY purchases.itemId
ORDER BY SUM(items.price) DESC
LIMIT 1
```
В таком примере использовать таблицу пользователей уже не требуется, поскольку неважно, кто именно купил товар

### Г) топ-3 товаров по выручке и их доля в общей выручке за любой год

Тоже не вполне однозначна формировка задачи. Требуется сделать выборку за любой произвольный год, или необходимо, чтобы в ответе присутствовали данные за все годы?

Для  какого-то определенного года:

``` PostgreSQL
SELECT items.itemId AS "id товара",  ROUND(CAST(SUM(items.price) AS numeric) /  
(
SELECT SUM(items.price) AS totalincome
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
WHERE EXTRACT(YEAR FROM purchases.date) = 2021
),2) AS proportion
FROM purchases 
LEFT JOIN items ON purchases.itemId = items.itemId 
WHERE EXTRACT(YEAR FROM purchases.date) = 2021
GROUP BY items.itemId
ORDER BY proportion DESC
LIMIT 3

``` 

CAST AS используем для совместимости

Для перебора по годам запрос получится сложным, поэтому используются CTE

``` PostgreSQL
WITH a AS (
SELECT itemId, extract('YEAR' FROM date) AS year, price
FROM Purchases
JOIN Items USING (itemId)
)
-- Это просто для удобства. Здесь просто получаем только те данные, что нам нужны - 
-- товар, год и цену
, b AS (
SELECT itemId, year, SUM(price)
FROM a
GROUP BY itemId, year
)
-- Здесь получаем выручку по товарам и годам 
, c AS (
SELECT year, SUM(price)
FROM a
GROUP BY year
-- Здесь получаем отдельно выручку только по годам - это нужно для "перебора" годов 
-- и получения общей суммы за год - для расчета доли
)
SELECT c.year::char(4), t1.itemId, ROUND((CAST(t1.sum AS numeric) / c.sum),2) AS ratio
FROM c
CROSS JOIN LATERAL (
SELECT b.itemId, b.sum
FROM b
WHERE b.year = c.year
ORDER BY sum DESC
LIMIT 3
) AS t1
ORDER BY year DESC, ratio DESC
``` 


<!-- [Ссылка на ресурс по PostgreSQL на русском](https://postgrespro.ru/)
-->
